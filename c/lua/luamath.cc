#include <stdio.h>
#include <errno.h>

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

lua_State* L;

/* call a function `f' defined in Lua */
double calc(double x, double y) {
    double z;

    /* push functions and arguments */
    lua_getglobal(L, "calc");  /* function to be called */
    lua_pushnumber(L, x);   /* push 1st argument */
    lua_pushnumber(L, y);   /* push 2nd argument */

    /* do the call (2 arguments, 1 result) */
    if (lua_pcall(L, 2, 1, 0) != 0)
        printf("error running function `calc': %s\n", lua_tostring(L, -1));

    /* retrieve result */
    if (!lua_isnumber(L, -1))
        printf("function `calc' must return a number\n");
    z = lua_tonumber(L, -1);
    lua_pop(L, 1);  /* pop returned value */
    return z;
}

int main(){
    double x, y;
    L = luaL_newstate();
    luaL_openlibs(L);

    if (luaL_loadfile(L, "math.lua") || lua_pcall(L, 0, 0, 0)) {
        printf("error: %s", lua_tostring(L, -1));
        return -1;
    }

    x = 3423;
    y = 12;

    printf("###: %lf\n", calc(x, y));
    
    lua_close(L);
    return 0 ;
}
