#include <stdio.h>
#include <math.h>

extern "C" {
	#include "lua.h"
	#include "lualib.h"
	#include "lauxlib.h"
}

/* the Lua interpreter */
lua_State* L;

static int average(lua_State *L)
{
	/* get number of arguments */
	int n = lua_gettop(L);
	double sum = 0;
	int i;

	/* loop through each argument */
	for (i = 1; i <= n; i++)
	{
		/* total the arguments */
		sum += lua_tonumber(L, i);
	}

	/* push the average */
	lua_pushnumber(L, sum / n);

	/* push the sum */
	lua_pushnumber(L, sum);

	/* return the number of results */
	return 2;
}


static int sumsqrt(lua_State *L){
    int n = lua_gettop(L);
    double sum = 0 , value; 
    int i ; 
    for(i = 1 ; i <= n ; ++ i)
    {
        value = lua_tonumber(L, i);
        sum += value * value;
    }
    lua_pushnumber(L, sqrt(sum)/n);
    return 1;
}

int main ( int argc, char *argv[] )
{
	/* initialize Lua */
	L = luaL_newstate();

	/* load Lua base libraries */
	luaL_openlibs(L);

	/* register our function */
	lua_register(L, "average", average);
	lua_register(L, "sqrtsum", sumsqrt);

	/* run the script */
	luaL_dofile(L, "avg.lua");

	/* cleanup Lua */
	lua_close(L);

	/* pause */
	printf( "Press enter to exit..." );
	getchar();

	return 0;
}
