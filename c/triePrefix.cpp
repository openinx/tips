#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

#define KEY_SIZE 26

struct Node{
    Node* children[KEY_SIZE];
    int weight;
    int cnt;
};

Node* newNode(){
    Node *n = new Node;
    for(int i = 0 ; i < KEY_SIZE; ++ i)
        n->children[i] = NULL;
    n->weight = 0 ;
    n->cnt = 0;
    return n;
}

class TriePrefix{
public:
    TriePrefix(){
        size = 0;
        root = newNode();
    }
    TriePrefix(_size){size = _size;}

    ~TriePrefix(){
    }

    void insert(char *s){
        Node *n = root;
        while(*s){
            int index = *s - 'a';
            if(n->children[index] == NULL ){
                n->children[index] = newNode();
            }
            n = n->children[index];
            n->weight ++;
            s ++ ;
        }
        n->cnt ++ ;
    }

    void _find(Node *root, int k, vector<string> &results){
        if(root->weight)

    }

    void search(char *prefix, int k, vector<string> &results){
        Node *n = root;
        while(*prefix && *(prefix+1)){
            int index = *prefix - 'a';
            if(n->children[index] == NULL)
                return
            n = n->children[index];
            prefix ++ ;
        }
        _find(n, k, results);
    }

private:
    int size;
    Node *root; 
};

/************************ Test *********************************************/

const static  string alpha = "abcdefghijklmnopqrstuvwxyz";
string randStr(int len){
    string s;
    for(int i = 0 ; i < len ; ++ i)
        s.push_back( alpha[ (rand() % alpha.size()) ] );
    return s;
}

void test(){
}

int main(){
    srand(time(NULL));
    cout << randStr(10) << endl;
    return 0 ;
}
