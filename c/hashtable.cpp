#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cassert>
#include <map>
#include <vector>

using namespace std;

class Entry{
public:
    int seq;
    string value;
    Entry(){}
    Entry(int _seq, string _value): seq(_seq), value(_value) {}
};

class HashTable{
public:
    HashTable(){
        global = 0; 
    }
    ~HashTable() {
        for(map<string, Entry*>::iterator it = ht.begin();
                it != ht.end(); it ++){
            Entry *e = it->second;
            delete e;
        }
    }

    int get(string key, string &value){
        if(ht.find(key) == ht.end()){
            return -1;
        }else{
            Entry *e = ht[ key ];
            if(allseq < e->seq ){
                value = e->value;
            }else{
                value = allvalue;
            }
        }
        return 0;
    }

    void set(string key, string value){
        Entry *e = NULL ;
        global = global + 1;
        if(ht.find(key) == ht.end()){
            e = new Entry(global, value);
            ht[key] = e;
            keys.push_back(key);
        }else{
            e = ht[key];
            e->seq = global;
            e->value = value;
            ht[key] = e;
        }
    }

    void setall(string value){
        global = global + 1;
        allseq = global;
        allvalue = value;
    }

    int getrand(string &key){
        if(keys.size() == 0){
            return -1;
        }else{
            key = keys[rand() % keys.size()];
            return 0;
        }
    }

    int size(){ return keys.size(); }
private:
    int global, allseq; 
    string allvalue;
    map<string, Entry*> ht;
    vector<string> keys;
};

/************************ Test *********************************************/

const static  string alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLUMNOPRSTUVWXYZ";
string randStr(int len){
    string s;
    for(int i = 0 ; i < len ; ++ i)
        s.push_back( alpha[ (rand() % alpha.size()) ] );
    return s;
}

#define MAX_KEY  10000
#define KEY_LEN  4

void test(){
    int rc;
    string key, value;
    map<string, string> kv;
    HashTable ht;
    for(int i = 0 ; i < MAX_KEY ; ++ i){
        int cmd = rand() % 4 ;
        if(cmd == 0){
            // get key
            key = randStr(KEY_LEN);
            int rc = ht.get(key, value);
            if(rc < 0){
                assert(kv.find(key) == kv.end());
            }else{
                assert(kv.find(key) != kv.end() || kv[key] == value);
            }
        }else if(cmd == 1){
            // set key value
            key = randStr(KEY_LEN);
            value = randStr(KEY_LEN);
            ht.set(key, value);
            kv[key] = value;
            assert(ht.size() == kv.size());
        }else if(cmd == 2){
            // setall value
            value = randStr(KEY_LEN);
            for(map<string,string>::iterator it = kv.begin(); it!=kv.end(); it++)
                kv[it->first] = value;
            ht.setall(value);
            assert(ht.size() == kv.size());
        }else if(cmd == 3){
            // getrand
            rc = ht.getrand(key);
            assert(rc < 0 || kv.find(key) != kv.end());
        }
    }
}

int main(){
    test();
    cout << "Run All Test Successfull." << endl;
    return 0 ;
}
