function listNew()
    return nil
end

function listPushFront(head, val)
    if not head then
        head = {next=nil, value=val}
        -- print('#####')
    else
        newNode = {next=head, value=val}
        head = newNode
    end
    -- print('****', head.value)
    return head
end

function listSize(head)
    local size
    local iter = head
    size = 0
    while iter do
        size = size + 1
        iter = iter.next
    end
    return size
end

function listFound(head, val)
    local iter = head
    while iter do
        if iter.value == val then
            return true
        else
            iter = iter.next
        end
    end
    return false
end

function listRemove(head, val)
    local iter = head
    local prev = nil
    while iter do
        if iter.value == val then
            break
        else
            prev = iter
            iter = iter.next
        end
    end
    if iter and iter.value == val then
        if prev == nil then
            head = head.next
        else
            prev.next = iter.next
        end
    end
    return head
end


function listPrint(head)
    local iter = head
    local size = 0
    while iter do
        print(string.format("list[%d] = %d", size, iter.value))
        iter = iter.next
        size = size + 1
    end
end

list = listNew()
maxn = 10


for i=1,maxn do
    list = listPushFront(list, i)
end

-- listPrint(list)
-- print(listFound(list, 1))

for i=1,maxn do
    assert(listFound(list, i), string.format('number %d can not be found in list.', i))
    list = listRemove(list,i)
    -- print(listSize(list))
    -- listPrint(list)
    assert(maxn - i == listSize(list))
end

-- listPrint(list)

