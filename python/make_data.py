import pymysql
import random
import string
import datetime
import itertools
import eventlet 
import time

eventlet.monkey_patch()

config = {
    'user': 'root',
    'host': '127.0.0.1',
    'pass': '',
    'port': 3306,
    'db': 'test',
    'rows': 1000000, 
    'threads': 1500,
    'rows_per_commit': 100,
}

create_table_sql = """
CREATE TABLE `fake_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `message_id` varchar(255) NOT NULL,
  `timeout` int(11) NOT NULL,
  `priority` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
"""

insert_sql = \
"insert into fake_data set created_at=%s, \
message_id=%s, timeout=%s, priority=%s"

def get_conn():
    return pymysql.connect(user=config['user'],
                           host=config['host'],
                           passwd=config['pass'],
                           port=config['port'], 
                           db=config['db'])

BASE_STR = string.lowercase + string.ascii_uppercase + string.digits
def randomword(length):
    return ''.join(random.choice(BASE_STR) for i in range(length))


def create_table():
    with get_conn() as cursor:
        try:
            cursor.execute(create_table_sql)
        except pymysql.err.InternalError as e:
            if e.args[0] != 1050:
                raise

BASE_FLOAT_ARRAY = [1.23343242342, 23.2332332232332,
                    3.2324313213413, 4.23423,
                    5.23423, 0.1234234,
                    1.234, 3.2342, 4.23424]

def make_data_task(task_id=0):
    print "Task %s started." % task_id
    conn = get_conn()
    BASE_STR_ARRAY = [randomword(20) for i in range(20)]
    with conn as cursor:
        for  i in itertools.count():
            if i >= config['rows']:
                break
            cursor.execute(insert_sql, [datetime.datetime.now(),
                                        random.choice(BASE_STR_ARRAY),
                                        10,
                                        random.choice(BASE_FLOAT_ARRAY)])
            if i % config['rows_per_commit'] == 0:
                conn.commit()
        conn.commit()


def main():
    create_table()
    task_size = config['threads']
    pool = eventlet.GreenPool(task_size)
    for task_id in range(task_size):
        pool.spawn(make_data_task, task_id)
        #time.sleep(random.uniform(0.01, 0.2))
    pool.waitall()

if __name__ == '__main__':
    main() 
