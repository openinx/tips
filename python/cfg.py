'''
Created on Nov 21, 2013

@author: hz
'''
import ConfigParser


class GroupNotFound(Exception):
    pass

class OptNotFound(Exception):
    pass


class Group(object):

    def __init__(self, config_parser, group_name):
        self._cp = config_parser
        self.group_name = group_name
        self._cache = {}

        self.auto_load()

    def auto_load(self):
        for option in self._cp.options(self.group_name):
            if option in self._cp.defaults():
                continue
            print '##', option, self.group_name
            self._cache[option] = self._cp.get(self.group_name, option)

    def __getattr__(self, key):
        if key in self._cache:
            return self._cache[key]
        raise OptNotFound('Group:%s Opt:%s' % (self.group_name, key))

    def list_opts(self):
        return self._cache


class Configure(object):

    def __init__(self, file_name):
        self.file_name = file_name
        self._cp = ConfigParser.ConfigParser()
        self._cache = {}
        self._defaults = {}
        self.auto_load()

    def auto_load(self):
        self._cp.read(self.file_name)
        self._defaults = self._cp.defaults()
        for group_name in self._cp.sections():
            group = Group(self._cp, group_name)
            self._cache[group_name] = group


    def __getattr__(self, key):
        if key in self._cache:
            return self._cache[key]
        if key in self._defaults:
            return self._defaults[key]
        raise GroupNotFound('group:%s' % key)

    def list_groups(self):
        return self._cache.values()


def main():
    cfg = Configure('xx.ini')
    print cfg.a
    print cfg.c
    print cfg.list_groups()
    for group in cfg.list_groups():
        print group.list_opts()


def get_default():
    cp = ConfigParser.ConfigParser()
    cp.read('xx.ini')
    print 'k' in cp.defaults()
    print cp.get(ConfigParser.DEFAULTSECT, 'a')
    print '###G', cp.options('mysql')

if __name__ == '__main__':
    main()
    # get_default()
