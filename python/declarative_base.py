'''
Created on Oct 21, 2013

@author: root


## QUERY 
//http://docs.sqlalchemy.org/en/rel_0_8/orm/tutorial.html#querying-with-joins
'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.sql.expression import func

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    addresses = relationship("Address", backref="user")

class Address(Base):
    __tablename__ = 'addresses'

    id = Column(Integer, primary_key=True)
    email = Column(String(50))
    user_id = Column(Integer, ForeignKey('users.id'))

some_engine = None
session = None

def get_session():
    global some_engine
    global session
    some_engine = create_engine('mysql://root:360buy@127.0.0.1:3306/test',
                                echo=True)
    SESSION = sessionmaker(bind=some_engine)
    session = SESSION(autocommit=True)
    return session

def ensure_create_table():
    global some_engine
    Base.metadata.create_all(bind=some_engine)

def add_user():
    user = User(name='huzheng123')
    # session.merge(user)
    address = Address()

    address.email = 'huzheng@jd.com'
    address.user_id = user.id

    user.addresses.append(address)
    session.add(user)
    session.add(address)
    session.flush()
    # session.commit()


def test_in():
    users = session.query(User).filter(User.id.in_([149, 160, 161])).all()
    for u in users:
        print u.id, u.name


def test_count():
    user_counts = session.query(func.count(User.id)).group_by(User.name).all()
    for u in user_counts:
        print u[0]

    us = session.query(func.count(func.distinct(User.name))).all()
    for u in us:
        print u[0]


def delete_user():
    pass


def delete_address():
    pass


def test_divide_page(filters, page=0, page_size=None):
    query = session.query(User).filter(**filters)
    if page_size:
        query = query.limit(page_size)
    if page:
        query = query.offset(page * page_size)
    return query


def list_user():
    users = session.query(User).all()
    for user in users:
        for addr in user.addresses:
            print "USER:", user.id, "ADDRESS:", addr.id, addr.email, addr.user_id


def list_address():
    addresses = session.query(Address).all()
    for addr in  addresses:
        print addr.id, "UserInformation:", addr.user.id, addr.user.name


class UserNotFound(Exception):
    pass

def add_addresses():
    user = session.query(User).filter_by(id=159).first()
    if not user:
        raise UserNotFound('user_id = %s' % user.id)
    address = Address()
    address.email = 'goodb@jd.com'
    # address.user_id = user.id
    user.addresses.append(address)
    # Flush to MYSQL
    # session.add(user)
    session.flush()


def add_my_user():
    user = User()
    user.name = 'hujintao'
    session.add(user)
    session.flush()




if __name__ == '__main__':
    get_session()
    ensure_create_table()

    # test_in()
    # test_count()
    # add_addresses()
    # add_my_user()

    # add_user()
    list_user()
    # list_address()
