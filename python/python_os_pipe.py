#!/usr/bin/python

import os
import subprocess
from subprocess import PIPE

r, w = os.pipe()

r = os.fdopen(r)
w = os.fdopen(w, 'w')

p = subprocess.Popen('tar xvf - -C  ~/test', close_fds=True, stdin=r, shell=True)

with open('./snapshot-2014-03-27_16:13:29.523973') as gd:
    while True:
        buf = gd.read(1024 * 16)
        if not buf:
            break
        w.write(buf)
    #MUST CLOSE.
    w.close()

result = p.communicate()

r.close()

if p.returncode:
    print 'ERROR', result
print 'OK.....'
