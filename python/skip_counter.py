#!/usr/bin/python

import pymysql
import time

conn = pymysql.connect(user='root',host='127.0.0.1')

index = 0

with conn as cursor:
    yes = 0
    while True:
            cursor.execute('start slave')
            time.sleep(0.2)
            x = cursor.execute('show slave status')
            try:
               ret = cursor.fetchall()[0]
            except:
               pass
            index += 1
            print '##SKIP_INDEX=', index, ret[37]
            if ret[11] == 'Yes':
                yes += 1
                if yes >= 10000:
                    break
                else:
                    continue
            else:
                cursor.execute('set global sql_slave_skip_counter = 1')
