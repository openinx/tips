#!/usr/bin/env python

# Copyright 2013 openinx@gmail.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import prettytable
import itertools
import traceback
import pymysql
import time

from optparse import OptionParser

try:
    import readline
    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set editing-mode emacs')
except:
    print 'Cannot ``import readline`` package.'


QUERY  = ["select", "desc", "show"]
UPDATE = ["insert", "delete"]
EXIT   = ["exit", "quit"]
USE    = ["use"]


def print_list(objs, fields, sortby_index=None):
    if sortby_index == None:
        sortby = None
    else:
        sortby = fields[sortby_index]
    pt = prettytable.PrettyTable([f for f in fields], caching=False)
    pt.align = 'l'

    for o in objs:
        row = []
        for field in fields:
            data = getattr(o, field, None)
            row.append(data)
        pt.add_row(row)

    print pt.get_string(sortby=sortby)


def print_dict(d, dict_property="Property"):
    pt = prettytable.PrettyTable([dict_property, 'Value'], caching=False)
    pt.align = 'l'
    [pt.add_row(list(r)) for r in d.iteritems()]
    print pt.get_string(sortby=dict_property)


class Row(dict):
    """A dict that allows for object-like property access syntax."""
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)


def connect(options):
    args = dict(user=options.user,
                passwd=options.passwd,
                host=options.host,
                port=options.port,
                db=options.db)
    return pymysql.Connect(**args)
    

def handle_pymysql_error(e):
    args = e.args
    print 'ERROR:%s %s' % (args[0], args[1])


class Operaters:

    TYPE_NONE    = "none"
    TYPE_QUERY   = "query"
    TYPE_UPDATE  = "update"
    TYPE_UNKONW  = "unknown"
    TYPE_EXIT    = "exit"
    TYPE_USE     = "use"

    def __init__(self, opts):
        self.opts = opts
        conn = None
        try:
            conn = connect(self.opts)
        except Exception, e:
            if isinstance(e, pymysql.Error):
                handle_pymysql_error(e)
                sys.exit(1)
        finally:
            if conn:
                conn.close()

    def none(self, sql, is_row):
        pass

    def query(self, sql, is_row):
        try:
            conn = connect(self.opts)
            cursor = conn.cursor()
            start_t = time.time()
            cursor.execute(sql)
            column_names = [d[0] for d in cursor.description]
            rows = [Row(itertools.izip(column_names, row)) for row in cursor]
            if is_row:
                print_list(rows, column_names)
            else:
                index = 0
                for row in rows:
                    index += 1
                    print '*' * 35, 'Row', index, '*' * 35
                    print_dict(row)
            end_t = time.time()
            print '%s row in set (%0.2f sec)' % (len(rows), end_t - start_t)
        except pymysql.Error, e:
            handle_pymysql_error(e)
        except Exception:
            traceback.print_exc()
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def _execute(self, sql, is_print=True):
        ret_code = 1
        try:
            conn = connect(self.opts)
            cursor = conn.cursor()
            start_t = time.time()
            rows = cursor.execute(sql)
            end_t = time.time()
            if is_print:
                print 'Query OK, %d rows affected (%0.2f sec)' % (
                                rows, end_t - start_t)
            conn.commit()
        except pymysql.Error, e:
            handle_pymysql_error(e)
            ret_code = 0
        except Exception:
            traceback.print_exc()
            ret_code = 0
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()
        return ret_code

    def update(self, sql, is_row):
        self._execute(sql)

    def use(self, sql, is_row):
        if self._execute(sql, is_print=False):
            self.opts.db = sql.strip().rpartition(' ')[2]

    def unknown(self, sql, is_row):
        print "Unknown SQL sentences, Ignore"

    def exit(self, sql, is_row):
        print "Bye"
        sys.exit(0)


def which_op(sql):
    is_row = True
    sql = sql.strip()
    if not sql:
        return sql, Operaters.TYPE_NONE, is_row
    sql = sql[:-1] if sql[-1] == ';' else sql
    if not sql:
        return sql, Operaters.TYPE_NONE, 0
    if len(sql) >= 2  and sql[-2] == '\\' and sql[-1].lower() == 'g':
        is_row = False
        sql = sql[:-2].strip()
        if not sql:
            return sql, Operaters.TYPE_NONE, is_row
    sql = sql[:-1] if sql[-1] == ';' else sql
    if not sql:
        return sql, Operaters.TYPE_NONE, is_row
    raw_key_op = sql.partition(' ')[0]
    key_op = raw_key_op.lower()
    if key_op in QUERY:
        return sql, Operaters.TYPE_QUERY, is_row
    if key_op in UPDATE:
        return sql, Operaters.TYPE_UPDATE, is_row
    if key_op in EXIT:
        return sql, Operaters.TYPE_EXIT, is_row
    if key_op in USE:
        return sql, Operaters.TYPE_USE, is_row
    #Unkonw SQL
    return sql, Operaters.TYPE_UNKONW, is_row


def _parse_args():
    parser = OptionParser()
    parser.add_option("-H", "--host", dest="host",
                      default="127.0.0.1",
                      type='str',
                      help="the host of server")
    parser.add_option("-P", "--port",
                      dest="port",
                      default=3306,
                      type='int',
                      help="the port of server")
    parser.add_option("-u", "--user",
                      dest="user",
                      default="root",
                      type='str',
                      help="mysql user")
    parser.add_option("-p", "--passwd",
                      dest="passwd",
                      default="",
                      type='str',
                      help="password for user to conect mysql server")
    parser.add_option("-D", "--db",
                      dest="db",
                      default=None,
                      type='str',
                      help="database to connect")
    (options, args) = parser.parse_args()
    return options, args


def main():
    (options, args) = _parse_args()
    operaters = Operaters(options)
    while True:
        raw_sql = raw_input("pysql> ")
        sql, op, is_row = which_op(raw_sql)
        func = getattr(operaters, op)
        func(sql, is_row)

if __name__ == '__main__':
    main()
