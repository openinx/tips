import subprocess
from subprocess import PIPE

attempts = 0
while True:
    cmd = 'sudo docker run -i -t ubuntu /bin/bash'
    p = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    retsult = p.communicate()
    if p.returncode == 0:
        break
    attempts += 1
    print 'attempts:%s stderr: %s' % (attempts, retsult[1])
