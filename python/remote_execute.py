 #!/usr/bin/python
#-*- coding: utf-8 -*-
#Filename: send_cmd.py

import sys
import paramiko

def send_cmd(ip,user,pwd,rootpwd):
        purge_cmd = create_cmd('cmdlist')
        port = 22
        paramiko.util.log_to_file('paramiko.log')
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip,username=user,password=pwd)
        chan = ssh.invoke_shell() 
        chan.send('su - \n')
        buff = ''
        line = 0
        while line < 4:
                resp = chan.recv(9999)
                buff += resp
                line +=1
                print '#', buff
        print 'buff:', buff
        print "send"
        chan.send(rootpwd)
        chan.send('\n')
        buff = ''
        while not buff.endswith('# '):
                print "##bbb: ",buff
                resp = chan.recv(9999)
                buff += resp
        chan.send(purge_cmd)
        chan.send('\n')
        buff = ''
        while not buff.endswith('# '):
                print "ccc"
                resp = chan.recv(9999)
                buff += resp
        print buff

        ssh.close()

def create_cmd(filename):
        purge_cmd = ""
        fff = open(filename)
        try:
                fffinfo=fff.read()
        finally:
                fff.close()
        purge_cmd = ''
        for url in fffinfo.split("\n"):
                if url:
                        purge_cmd = purge_cmd + url +";"
                return purge_cmd

def main():
        filename = sys.argv[1]
        file = open(filename)
        try:
                hostlist = file.read()
        finally:
                file.close()
        for host in hostlist.split("\n"):
                if host:
                        ip,user,pwd,rootpwd = host.split(",")
                        print ip
                        print "\n"
                        send_cmd(ip,user,pwd,rootpwd)

if __name__ == '__main__':
        main()
