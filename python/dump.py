import pymysql
import subprocess
from subprocess import PIPE

exclude_dbs = ['mysql', 'information_schema', 'performance_schema', 'tmpdir', 'test']
conn = pymysql.connect(user='root', host='127.0.0.1', port=3306)
with conn as cursor:
    cursor.execute('show databases')
    databases = [row[0] for row in cursor if row[0] not in exclude_dbs ]

for db in databases:
    cmd = 'mysqldump -uroot -h127.0.0.1 %s > %s.sql ' % (db, db)
    p = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
    stdout, stderr = p.communicate()
    if stderr:
        print 'ERROR:', stderr
    else:
        print 'mysqldump %s finished'  % db
