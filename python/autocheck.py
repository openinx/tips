import eventlet
import pymssql
import pymysql
import time

import comparator

config = {
    'source': {
        'type': 'sqlserver',
        'user': 'xxxx',
        'password': 'xxxxxxxx',
        'host': '192.168.194.22',
        'port': 1433,
        'database': 'test',
        'charset': 'utf8',
    },
    'destination': {
        'type': 'mysql',
        'user': 'test',
        'password': '1q2w3e4r',
        'host': '192.168.194.23',
        'port': 3306,
        'database': 'test',
        'charset': 'utf8',
    },
    'tables': [ 'testdata' ],
    'threads': 100,
    'rows_per_thread': 100,
}

def get_msconn():
    cfg = config['source']
    return pymssql.connect(user=cfg['user'],
                           password=cfg['password'],
                           host=cfg['host'],
                           port=cfg['port'],
                           charset=cfg['charset'],
                           database=cfg['database'])

def get_myconn():
    cfg = config['destination']
    return pymysql.connect(user=cfg['user'],
                           passwd=cfg['password'],
                           host=cfg['host'],
                           port=cfg['port'],
                           charset=cfg['charset'],
                           database=cfg['database'])


def get_ms_table_pk(table_name):
    sql_str = """
        SELECT Col.Column_Name from
        INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab,
        INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col
        WHERE
        Col.Constraint_Name = Tab.Constraint_Name
        AND Col.Table_Name = Tab.Table_Name
        AND Constraint_Type = 'PRIMARY KEY '
        AND Col.Table_Name = '%s'
    """ % table_name
    msconn = get_msconn()
    try:
        cursor = msconn.cursor()
        cursor.execute(sql_str)
        return [row[1] for row in cursor ]
    finally:
        msconn.close()


def get_my_table_pk(table_name):
    sql_str = "desc %s" % table_name
    myconn = get_myconn()
    with myconn as cursor:
        cursor.execute(sql_str)
    return [ row[0] for row in cursor if row[3] == 'PRI']


def get_ms_table_count(table_name):
    sql_str = "select count(*) from %s" % table_name
    msconn = get_msconn()
    try:
        cursor = msconn.cursor()
        cursor.execute(sql_str)
        return sum([ row[0] for row in cursor ])
    finally:
        msconn.close()


def get_my_table_count(table_name):
    sql_str = "select count(*) from %s" % table_name
    myconn = get_myconn()
    with myconn as cursor:
        cursor.execute(sql_str)
    return sum([row[0] for row in cursor])


def row_equal(a, b):
    for k, v in a.iteritems():
        if k in b and b[k] != v:
            return False, k
    return True, None

def task_cmp(pool, task_id, table_name, primary_keys, pk_rows):
    start_t = time.time()
    sql_str = "select * from %s where " % table_name
    ms_sql = sql_str + " and ".join([pk + '=%s' for pk in primary_keys ])
    my_sql = sql_str + " and ".join([pk + '=%s' for pk in primary_keys ])

    def get_ms_data():
        #print 'task-%s get ms data' % task_id
        msconn = get_msconn()
        rows = []
        try:
            cursor = msconn.cursor()
            for pk_row in pk_rows:
                cursor.execute(ms_sql, pk_row)
                colnames = [i[0] for i in cursor.description]
                row = dict(zip(colnames, cursor.fetchone()))
                rows.append(row)
            #print 'task-%s, len(rows):%s' % (task_id, len(rows))
        finally:
            msconn.close()
        return rows

    def get_my_data():
        #print 'task-%s get my data' % task_id
        myconn = get_myconn()
        rows = []
        with myconn as cursor:
            for pk_row in pk_rows:
                cursor.execute(my_sql, pk_row)
                colnames = [i[0] for i in cursor.description]
                row = dict(zip(colnames, cursor.fetchone()))
                rows.append(row)
        return rows

    #ms_gt = eventlet.greenthread.spawn(get_ms_data)
    #my_gt = eventlet.greenthread.spawn(get_my_data)
    ms_gt = pool.spawn(get_ms_data)
    my_gt = pool.spawn(get_my_data)

    ms_rows = ms_gt.wait()
    my_rows = my_gt.wait()

    #print '######'
    if len(ms_rows) != len(my_rows):
        #TODO row count is not equal
        print "rows count is not equal" , 
        pass
    else:
        for a, b in zip(ms_rows, my_rows):
            flag, key = row_equal(a, b)
            if not flag:
                print 'key is not equal', key
    end_t = time.time()
    print 'task-id: %s rows :%s take %s' % (task_id, len(pk_rows), end_t - start_t)

def start_cmp(pool, table_name):
    primary_keys = get_my_table_pk(table_name)
    pk_strs = ",".join(primary_keys)
    sql_str = "select %s from %s" % (pk_strs, table_name)
    msconn = get_msconn()
    cursor = msconn.cursor()
    try:
        cursor.execute(sql_str)
        idx = 0
        pk_rows = []
        task_id = 0
        for row in cursor:
            idx += 1
            pk_rows.append(row)
            if idx >= config['rows_per_thread']:
                idx = 0
                task_id += 1
                #print 'start task %s' % task_id
                pool.spawn(task_cmp, pool, task_id, table_name, primary_keys, pk_rows)
                pk_rows = []
    finally:
        msconn.close()
    #Deal with remaind record.
    if idx != 0:
        task_id += 1
        pool.spawn(task_cmp, pool, task_id, table_name, primary_keys, pk_rows)
        pk_rows = []
    pool.waitall()


def main():
    pool = eventlet.GreenPool(config['threads'])
    for table_name in config['tables']:
        ms_gt = pool.spawn(get_ms_table_count, table_name)
        my_gt = pool.spawn(get_my_table_count, table_name)
        ms_count = ms_gt.wait()
        my_count = my_gt.wait()
        start_cmp(pool, table_name)
        return
        if ms_count == my_count:
            start_cmp(pool, table_name)
        else:
            print 'table:%s, mysql count:%s, ms_count:%s' % \
                    (table_name, ms_count, my_count)

if __name__ == '__main__':
    main()
