'''
Created on Mar 3, 2014

@author: hz
'''

import os
import pymysql
import time
import subprocess
from jss.connection import JssClient
from jss.ext.client import CompressObjectPut

config = {
        'access_key': 'test',
        'secret_key': 'test',
        'host': 'storage.jd.com',
        'port': 80,
        'bucket': 'test',
        'object': 'test',
        'mysql_user': 'test',
        'mysql_passwd': '1234567',
        'mysql_host': 'test',
        'mysql_port': 3358,
        'mysql_db': 'test',
}

def get_local_ip():
    shell_cmd = "ifconfig |grep inet|grep -v '127.0.0.1'|sed -n '1p'|awk '{print $2}'|awk -F ':' '{print $2}'"
    out = subprocess.check_output(shell_cmd, shell=True)
    IP = out.strip("\n").strip()
    return IP


with open('./data.txt') as fd:
    for line in fd:
        if line.strip():
            aid = line.split()[1][-7:]
            with pymysql.connect(user=config['mysql_user'],
                                 passwd=config['mysql_passwd'],
                                 host=config['mysql_host'],
                                 port=config['mysql_port'],
                                 db=config['mysql_db']) as cursor:
                cursor.execute('select object_name, host, name from binlog where aid = %s' % aid)
                row = cursor.fetchone()
                object_name = row[0]
                host = row[1]
                if host != get_local_ip():
                    continue

                binlog_path = '/database/log/%s' % row[2]
                if not os.path.exists(binlog_path):
                    print 'ERROR: ', 'Binlog', row[2], 'has been areadly deleted.'
                    continue

                config['object'] = object_name

                # Delete the object
                i = 0
                delay = 0.1

                while True:
                    try:
                        try:
                            jssclient = JssClient(config['access_key'], config['secret_key'], config['host'], config['port'])
                            jssclient.bucket(config['bucket']).delete_key(config['object'])
                        except Exception as e:
                            if "NoSuchKey" in e.args[0]:
                                pass
                            else:
                                print 'ERROR: %s' % e.args[0]

                        if i >= 1:
                            print 'attempt retries: %s object:%s' % (i, object_name)

                        # PUT the object
                        put_cli = CompressObjectPut(jssclient, bucket_name=config['bucket'], object_name=config['object'])

                        put_cli.start()
                        with open(binlog_path) as fd:
                            while True:
                                data = fd.read(16 * 1024 * 1024)
                                if not data:
                                    break
                                put_cli.write(data)
                            put_cli.end(nowait=False)
                        print host, ' : ', config['object'], 'finished'
                        break
                    except:
                        if i >= 8:
                            raise
                        i += 1
                        time.sleep(delay)
                        delay *= 2
                    else:
                        break
