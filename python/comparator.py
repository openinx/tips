#!/usr/bin/python
#coding=utf-8


import traceback
import logging

from decimal import Decimal

supported_types = ['Decimal', 'str', 'int',  'long', 'datetime', 'unicode', 'float', 'bool', 'NoneType']

supported_charset = [ 'utf8', 'gbk', 'ascii', 'cp936', 'UTF-8', 'UTF-16',
                      'UTF-32', 'ISO-8859-9', 'gb2312', 'gb18030',
                      'BIG5', 'ISO-8859-1', 'windows-1250',
                      'windows-1252']

int_types = ['int', 'long']
bool_types = ['int', 'long', 'bool']


LOG = logging

def _t(a):
    return type(a)


def unicode_str_eq(unicode_var, str_var):
    try:
        if str(unicode_var) == str_var:
            return True
    except:
        pass
    for charset in supported_charset:
        try:
            if str_var.decode(charset) == unicode_var:
                return True
        except:
            pass
    return False


def to_unicode(s, chs):
    if isinstance(s, unicode):
        return s
    try:
        s = s.decode(chs)
    except:
        pass
    return s


class Comparator:

    def basestring_eq(self, a, b):
        if isinstance(a, unicode) and isinstance(b, unicode):
            return a == b
        for cs1 in supported_charset:
            for cs2 in supported_charset:
                xa =  to_unicode(a, cs1)
                xb =  to_unicode(b, cs2)
                if isinstance(xa, unicode) and isinstance(xb, unicode):
                    #print 'AAAAAAAA', a
                    #print 'BBBBBBBB', b
                    if xa.strip() == xb.strip():
                        return True
        #print 'a', 'b', 'diff'
        return False

    def unicode(self, a, b):
        return self.basestring_eq(a, b)

    def str_eq(self, a, b):
        return self.basestring_eq(a.strip(), b.strip())

    def int_eq(self, a, b):
        return a == b

    def datetime_eq(self, a, b):
        return a.year == b.year and \
               a.month == b.month and \
               a.day == b.day and \
               a.hour == b.hour and \
               a.minute == b.minute and \
               a.second == b.second

    def Decimal_eq(self, a, b):
        return abs(a - b) < 1e-9

    def float_eq(self, a, b):
        return abs(a - b) < 1e-9

    def bool_eq(self, a, b):
        return a == b

    def NoneType_eq(self, a, b):
        #print 'hello world'
        return True

    def eq(self, a, b):
        ta = type(a).__name__
        tb = type(b).__name__
        if ta not in supported_types or tb not in supported_types:
           # print 'type not supoorted'
            return False
        #Integer
        if ta in int_types and tb in int_types:
            #print 'int type not eq'
            return self.int_eq(a, b)
        #String
        if isinstance(a, basestring) and isinstance(b, basestring):
            #print 'string type not eq'
            return self.basestring_eq(a.strip(), b.strip())
        #Boolean
        if ta in bool_types and tb in bool_types:
            #print 'bool type not eq'
            return self.bool_eq(a, b)
        if ta != tb:
            #print 'type not eq'
            return False
        type_eq = getattr(self, ta + '_eq')
        return type_eq(a, b)


if __name__ == '__name__':
    main()
