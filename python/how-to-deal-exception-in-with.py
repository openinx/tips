"""
How to print the full stack of exception?

Output:

####Deal with something , when encounter a exception...
Traceback (most recent call last):
  File "gdd.py", line 35, in <module>
    hello()
  File "gdd.py", line 35, in <module>
    hello()
  File "gdd.py", line 20, in hello
    g()
  File "gdd.py", line 12, in g
    h()
  File "gdd.py", line 7, in h
    raise Exception('h')
Exception: h

"""
import sys

def h():
    raise Exception('h')
    
# OUT: (<type 'exceptions.Exception'>, Exception('h',), <traceback object at 0x24a5488>)
def g():
    try:
        h()
    except:
        exc, value , tb = sys.exc_info()
        if exc:
            raise


def hello():
    g()


class WithObject:

    def __enter__(self):
        return self

    def __exit__(self, exc, value, tb):
        print '####Deal with something , when encounter a exception...'
        if exc:
            raise


with WithObject() as withobj:
    hello()
