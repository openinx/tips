Tips for Linux Programming
==========================

### How to remove some target files under current directory ? 
```shell
    find . -name ".git*" -exec rm -rf {} \;
```

### How to show infomation of your LINUX machine?
```shell
uname -r # system version
uname -a 
cat /proc/cpuinfo
cat /proc/meminfo
more /etc/issue
more /proc/version
lsb_release -a 
```

### apt-cache
```shell
apt-cache show <package-name>
apt-cache search <package-name>
apt-cache showpkg <package-name> # show dependecy
apt-cache stats
apt-cache depends <package-name>
```

### transfor  big files  between servers

```shell
#server
cd /path/to/your/directory
tar cvf - * | nc -l 8899
```

```shell
#client
cd /path/to/your/directory
nc 192.168.194.121 8899 | tar xvf - 
```

### Port sweeping
```shell
nc -v -w 2 192.168.2.34 -z 21-24
```
### How to get current module in current file ? 
```python
import sys
print __name__
print sys.modules[__name__]
```

### how to find where the python package source code is 
```shell
python -c 'print __import__("novaclient").__file__' 
```



iftop


### Add route 
```shell
ip r a 172.17.25.12/32 via 172.17.35.254  dev eth3
```


### Grant Privileges for MYSLQ USE
```shell
mysql> GRANT ALL ON glance.* TO 'glance'@'%' IDENTIFIED BY '[YOUR_GLANCEDB_PASSWORD]';
mysql> GRANT ALL ON glance.* TO 'glance'@'localhost' IDENTIFIED BY '[YOUR_GLANCEDB_PASSWORD]';
```



###  TAR SHELL
```shell
tar cvf - * | qpress -iov data | nc -l 8888
nc ip 8888 | qpress -diov | tar xvf - 
```


### VIM 
```
yiw # copy the word where cursor at 
viw # select word where cursor at
```


### Find the biggist file in DIR
```
du -hsx * | sort -rh | head -10
find / -size +204800 
```

### Grant a new Slave
```python
GRANT SELECT, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO  abc@'target-host' IDENTIFIED BY 'abcabc'  ;
```


### Sysbench
```python
sysbench --test=oltp --mysql-table-engine=innodb  --oltp-table-size=80000000  --mysql-user=huzheng --mysql-host=192.168.33.53 --mysql-port=3306 --mysql-db=test --mysql-password=360buy12345 --db-driver=mysql   prepare
sysbench --test=oltp --mysql-table-engine=innodb  --oltp-table-size=80000000  --mysql-user=huzheng --mysql-host=192.168.33.53 --mysql-port=3306 --mysql-db=test --mysql-password=360buy12345 --db-driver=mysql   prepare
```
