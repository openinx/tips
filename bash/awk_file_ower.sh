#!/bin/bash


#
# print $File\t$Owner
#       file1   owner1
#       file2   owner2

# Link : http://www.grymoire.com/Unix/Awk.html#uh-8

ls -l | awk '
BEGIN { printf("File\tOwner\n") }
{ printf ("%s\t%s\n", $9, $3)}
END { printf("---DONE---\n")}
'
