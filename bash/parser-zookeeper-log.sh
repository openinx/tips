#!/bin/bash

ZOO_HOME="/home/nepo/soft/zookeeper-3.4.5"

java -cp $ZOO_HOME/zookeeper-3.4.5.jar:$ZOO_HOME/lib/slf4j-log4j12-1.6.1.jar:$ZOO_HOME/lib/slf4j-api-1.6.1.jar:$ZOO_HOME/lib/log4j-1.2.15.jar org.apache.zookeeper.server.LogFormatter $1
