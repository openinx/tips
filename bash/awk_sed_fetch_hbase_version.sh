#!/bin/bash

#
# fetch hbase version by region server list.
# export hbase region server list to "dd.log", dd.log example:
#  c3-hadoop-prc-st318.bj,28600,1473217480977
#  c3-hadoop-prc-st318.bj,28610,1473217480977

for host in $(cat  dd.log  | awk -F ',' '{printf("%s:%s\n", $1,$2+1)}'); do 
    echo $host=`curl -X GET -i http://$host/rs-status 2>/dev/null | grep -i 'HBase Version'`
done   | sed 's/\(.*\)=<tr><td>HBase Version<\/td><td>\(.*\)<\/td><td>HBase\(.*\)$/\1=\2/g'
