#!/bin/bash

yum -y install salt-minion

IP_SALT_MASTER="192.168.194.131"
IP_ETH0=`ifconfig |grep inet|grep -v "127.0.0.1"|sed -n '1p'|awk '{print $2}'|awk -F ':' '{print $2}'`

sed -i "s/#master: salt/master: $IP_SALT_MASTER/g" /etc/salt/minion
sed -i "s/#id:/id: mysql-$IP_ETH0/g" /etc/salt/minion

service salt-minion start
