#!/bin/bash

# caculate the prime in [1,100000]
SIZE=100
for i in `seq $SIZE`; do 
    flag[i]=1
done

flag[0]=0
flag[1]=0

for i in `seq 2 $SIZE`; do
    if [ ${flag[$i]} == 1 ];then
        #2>/dev/null to ignore warning
        #  'seq: needs negative decrement'
        for j in `seq $(($i+$i)) $i $SIZE 2>/dev/null`; do 
            flag[$j]=0
        done
    fi
done

for i in `seq 2 $SIZE`; do
    if [ ${flag[$i]} == 1 ]; then
        echo $i
    fi
done
