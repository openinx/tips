#!/bin/bash

# find all processingTime scan > 10 seconds in hbase.43717.log
# by using awk if > 100000 then print syntax
# other points like:
#   1. index function
#   2. substr function
#   3. length function
#   4. if condition
#   5. turn a string to integer , by +0

ls -tr | grep hbase.43717  | xargs grep 'processingTime' | awk '{t=substr($10,index($10,"processingTime=")+15)+0; if(t>100000) print $0}'  >  huge-process-time.log
