#!/bin/bash

# calculate the sum bytes of target directory.

DIR=.

sum=0
for file in $(find $DIR); do
    if [ -f $file ]; then
        size=`ls -l $file | awk '{print $5}'`
        sum=$(($sum+$size))
    fi
done

echo $sum
