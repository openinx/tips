package algorithm;

import static org.junit.Assert.assertEquals;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;


public class SkiplistTest {
  
  public String toString(Set<Integer> set) {
    StringBuilder builder = new StringBuilder();
    builder.append("size=" + set.size() + "[");
    int index = 0 ;
    for (Integer in : set) {
      if(index > 0){
        builder.append(',');
      }
      builder.append(in);
    }
    builder.append("]");
    return builder.toString();
  }
  
  @Test
  public void testAPI() {
    Skiplist list = new Skiplist();
    Set<Integer> set = new TreeSet<Integer>();
    Random rand = new Random(System.currentTimeMillis());
    for (int i = 0; i < 1000000; ++i) {
      int val = rand.nextInt(10000000);
      set.add(val);
      list.add(val);
      assertEquals(set.size(), list.size());
    }

    Object[] setArray = set.toArray();
    int[] listArray = list.toArray();
    assertEquals(setArray.length, listArray.length);

    for (int i = 0; i < setArray.length; ++i) {
      assertEquals((Integer) setArray[i], (Integer) listArray[i]);
    }

    for (int i = 0; i < listArray.length; ++i) {
      if (set.contains(setArray[i])) {
        assertEquals(list.remove(listArray[i]), true);
        assertEquals(set.remove(setArray[i]), true);
      }
    }

    assertEquals(list.size(), 0);
    assertEquals(set.size(), 0);

    if (list.size() < 1000) {
      assertEquals(list.toString(), toString(set));
    }
  }
}
