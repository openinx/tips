package algorithm;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RadixTreeTest {

  @Test
  public void testAPI() {
    RadixTree<String> tree = new RadixTree<String>();

    tree.insert("hello", "world");
    tree.insert("hello1", "world1");
    tree.insert("hello2", "world2");
    tree.insert("he", "world2");
    tree.insert("her", "world2");
    tree.insert("his", "world2");
    tree.insert("ruby", "world2");
    tree.insert("rudaf", "world2");
    tree.insert("cao", "world2");
    tree.insert("cahuo", "world2");
    tree.insert("doubi", "world2");
    tree.insert("doubility", "world2");
    tree.insert("dd", "world2");
    
    int count = 13;

    assertEquals(tree.size(), count);

    tree.remove("he");
    assertEquals(tree.get("he"), null);
    assertEquals(tree.contains("he"), false);
    assertEquals(tree.size(), --count);

    tree.remove("hello2");
    assertEquals(tree.get("hello2"), null);
    assertEquals(tree.contains("hello2"), false);
    assertEquals(tree.size(), --count);

    tree.insert("", "goodboy");
    assertEquals(tree.get(""), "goodboy");
    assertEquals(tree.contains(""), true);
    assertEquals(tree.size(), ++count);

    tree.insert("hello1", "world3");
    assertEquals(tree.size(), count);
    assertEquals(tree.get("hello1"), "world3");

  }
}
