package algorithm;

import java.util.Random;

public class Vector {

  private int count;
  private int[] array;

  Vector() {
    count = 0;
    array = new int[4];
  }

  Vector(int[] other) {
    int arraySize = 4;
    while (other.length < arraySize)
      arraySize *= 2;
    count = 0;
    array = new int[arraySize];
    System.arraycopy(other, 0, array, 0, other.length);
  }

  public int size() {
    return count;
  }

  private void extend() {
    int[] newArray = new int[array.length * 2];
    for(int i = 0 ; i < array.length ; ++ i)
      newArray[i] = array[i];
    array = newArray;
  }

  private void shrink() {
    if (count < array.length / 2) {
      int[] newArray = new int[array.length / 2];
      for (int i = 0; i < count; ++i) {
        newArray[i] = array[i];
      }
      array = newArray;
    }
  }

  public void pushBack(int value) {
    if (count >= array.length) {
      extend();
    }
    array[count++] = value;
  }

  public int search(int value) {
    for (int i = 0; i < count; ++i) {
      if (array[i] == value) {
        return i;
      }
    }
    return -1;
  }

  public int indexAt(int index) {
    if (index < 0 || index >= count) {
      throw new ArrayIndexOutOfBoundsException("index: " + index + " is out of (" + 0 + "," + count
          + ")");
    }
    return array[index];
  }

  public void remove(int value) {
    int index = search(value);
    if (index < 0) {
      return;
    }
    for(int i = index + 1 ; i < array.length ; ++i)
      array[i - 1] = array[i];
    count--;
    shrink();
  }

  public int pop() {
    if (count <= 0) {
      throw new ArrayIndexOutOfBoundsException("index: " + count + "is out of (" + 0 + "," + count
          + ")");
    }
    return array[--count];
  }

  public void sort() {
    for (int i = 0; i < count; ++i) {
      int minIndex = i;
      for(int j = i + 1; j < count ; ++ j)
        if (array[j] < array[minIndex]) {
          minIndex = j;
        }
      if(minIndex != i){
	      int temp = array[minIndex];
        array[minIndex] = array[i];
        array[i] = temp;
      }
    }
  }

  public void append(Vector other) {
    for (int i = 0; i < other.size(); ++i) {
      this.pushBack(other.indexAt(i));
    }
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Vector(count=" + count + ", size="+ array.length +")[");
    for (int i = 0; i < count; ++i) {
      if (i != 0) {
        builder.append(",");
      }
       builder.append(array[i]);
    }
    builder.append("]");
    return builder.toString();
  }

  public static void main(String args[]) {
    Random rand = new Random();
    Vector v = new Vector();
    for (int i = 0; i < 1000; ++i) {
      int value = rand.nextInt(100000) + 1;
      v.pushBack(value);
    }
    v.sort();
    for (int i = 0; i < 888; ++i)
      v.pop();
    System.out.println(v.toString());
  }
}
