package algorithm;

public class leetcodeMaxProductSubArray {

  public int maxProduct(int[] A) {
    int max = A[0];
    int min = A[0];
    int ret = A[0];
    for (int i = 1; i < A.length; ++i) {
      int a = max * A[i];
      int b = min * A[i];
      int c = A[i];
      max = Math.max(Math.max(a, b), c);
      min = Math.min(Math.min(a, b), c);
      ret = Math.max(ret, max);
    }
    return ret;
  }

  public static void main(String args[]) {
    int[] a = new int[] { 2, 3, -2, 4 };
    leetcodeMaxProductSubArray test = new leetcodeMaxProductSubArray();
    System.out.println(test.maxProduct(a));

    int[] b = new int[] { 0, 0, 0 };
    System.out.println(test.maxProduct(b));

    int[] c = new int[] { 2, 3, -2, 4, 2, 0 };
    System.out.println(test.maxProduct(c));

    int[] d = new int[] { 2 };
    System.out.println(test.maxProduct(d));
  }

}
