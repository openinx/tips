package algorithm;

import java.util.ArrayList;

class RadixNode<T> {
  String prefix;
  T value;
  ArrayList<RadixNode<T>> children;
  boolean real;

  RadixNode() {
    prefix = "";
    children = new ArrayList<RadixNode<T>>();
    real = false;
    value = null;
  }

  RadixNode(String prefix, T value, ArrayList<RadixNode<T>> children, boolean real) {
    this.prefix = prefix;
    this.value = value;
    this.children = children;
    this.real = real;
  }

  int getPrefixSize(String otherKey) {
    int i = 0;
    while (i < prefix.length() && i < otherKey.length()) {
      if (prefix.charAt(i) != otherKey.charAt(i)) {
        break;
      }
      i++;
    }
    return i;
  }
}

public class RadixTree<T> {
  private RadixNode<T> root;
  private int elemSize;
  
  public RadixTree() {
    root = new RadixNode<T>();
    elemSize = 0;
  }

  public void insert(String key, T value) {
    boolean inserted = insert(root, key, value);
    elemSize = inserted ? elemSize + 1 : elemSize;
  }

  private boolean insert(RadixNode<T> root, String key, T value) {
    boolean inserted = false;
    if (key.length() == 0) {
      inserted = root.real == false;
      root.real = true;
      root.value = value;
    } else {
      for (RadixNode<T> child : root.children) {
        int len = child.getPrefixSize(key);
        if (len > 0) {
          if (len == child.prefix.length()) {
            return insert(child, key.substring(len), value);
          } else {
            RadixNode<T> nodeForChild =
                new RadixNode<T>(child.prefix.substring(len), child.value, child.children,
                    child.real);
            RadixNode<T> nodeForKey =
                new RadixNode<T>(key.substring(len), value, new ArrayList<RadixNode<T>>(), true);
            child.prefix = key.substring(0, len);
            child.children = new ArrayList<RadixNode<T>>();
            child.children.add(nodeForChild);
            if (nodeForKey.prefix.length() != 0) {
              child.children.add(nodeForKey);
              child.real = false;
            } else {
              child.real = true;
              child.value = value;
            }
            return true;
          }
        }
      }
      RadixNode<T> newNode = new RadixNode<T>(key, value, new ArrayList<RadixNode<T>>(), true);
      root.children.add(newNode);
      inserted = true;
    }
    return inserted;
  }

  public void remove(String key) {
    String s = new String(key);
    RadixNode<T> parent = null, x = root;
    do {
      int len = x.getPrefixSize(s);
      if (len == s.length()) {
        if (x.real) {
          x.real = false;
          --elemSize;
          if (parent != null && x.children.size() == 0) {
            parent.children.remove(x);
          }
        }
        return;
      }
      s = s.substring(len);
      for (RadixNode<T> child : x.children) {
        if (child.prefix.length() > 0 && s.charAt(0) == child.prefix.charAt(0)) {
          x = child;
          parent = x;
          break;
        }
      }
    } while (true);
  }

  public T get(String key) {
    String s = new String(key);
    RadixNode<T> x = root;
    do{
      int len = x.getPrefixSize(s);
      if(len == s.length()){
        if(x.real){
          return x.value;
        }
        return null;
      }
      s = s.substring(len);
      for (RadixNode<T> child : x.children) {
        if (child.prefix.length() > 0 && s.charAt(0) == child.prefix.charAt(0)) {
          x = child;
          break;
        }
      }
    } while (true);
  }
  
  public boolean contains(String key) {
    String s = new String(key);
    RadixNode<T> x = root;
    do {
      int len = x.getPrefixSize(s);
      if (len == s.length()) {
        if (x.real) {
          return true;
        }
        return false;
      }
      s = s.substring(len);
      for (RadixNode<T> child : x.children) {
        if (child.prefix.length() > 0 && s.charAt(0) == child.prefix.charAt(0)) {
          x = child;
          break;
        }
      }
    } while (true);
  }

  public int size() {
    return elemSize;
  }

  private void format(StringBuilder b, int level, RadixNode<T> node) {
    for(int i = 0  ; i < level ; ++ i)
      b.append(' ');
    b.append('|');
    for (int i = 0; i < level; ++i)
      b.append('-');
    if (node.real) {
      b.append(node.prefix);
      b.append('[');
      b.append(node.value.toString());
      b.append("]*");
    } else {
      b.append(node.prefix);
    }
    b.append('\n');
    for(RadixNode<T> child: node.children){
      format(b, level + 1, child);
    }
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    format(builder, 0, root);
    return builder.toString();
  }

}
