package algorithm;

import java.util.Random;

class SkipNode {
  int key;
  SkipNode[] forward;

  SkipNode(int value, int height) {
    this.key = value;
    this.forward = new SkipNode[height];
  }
}

public class Skiplist {

  public static final int MAX_HEIGHT = 32;
  public static final double PB = 0.25;


  private int level, elemSize;
  private SkipNode head;
  private Random rand;

  public Skiplist() {
    level = 1;
    head = new SkipNode(0, MAX_HEIGHT);
    rand = new Random(System.currentTimeMillis());
    elemSize = 0;
  }

  private int randomLevel() {
    int level = 1;
    while (rand.nextDouble() < PB)
      level += 1;
    return (level < MAX_HEIGHT ? level : MAX_HEIGHT);
  }

  public boolean get(int key) {
    SkipNode x = head;
    for (int i = level - 1; i >= 0; --i) {
      while(x.forward[i] != null && x.forward[i].key < key)
        x = x.forward[i];
      if (x.forward[i] != null && x.forward[i].key == key) {
        return true;
      }
    }
    return false;
  }

  public void add(int key) {
    SkipNode x = head;
    SkipNode update[] = new SkipNode[MAX_HEIGHT];
    for(int i = level -1 ; i>= 0 ; --i) {
      while (x.forward[i] != null && x.forward[i].key < key)
        x = x.forward[i];
      if (x.forward[i] != null && x.forward[i].key == key) {
        return;
      }
      update[i] = x;
    }
    int newLevel = randomLevel();
    if (level < newLevel) {
      for (int i = level; i < newLevel; ++i) {
        update[i] = head;
      }
      level = newLevel;
    }
    SkipNode newNode = new SkipNode(key, newLevel);
    for (int i = 0; i < newLevel; ++i) {
      newNode.forward[i] = update[i].forward[i];
      update[i].forward[i] = newNode;
    }
    elemSize++;
  }

  public boolean remove(int key) {
    SkipNode x = head;
    SkipNode update[] = new SkipNode[MAX_HEIGHT];
    for (int i = level - 1; i >= 0; --i) {
      while(x.forward[i] != null && x.forward[i].key < key)
        x = x.forward[i];
      update[i] = x;
    }
    x = x.forward[0];
    if (x != null && x.key == key) {
      for (int i = 0; i < x.forward.length; ++i)
        update[i].forward[i] = x.forward[i];
    } else {
      return false;
    }
    while (level > 1 && head.forward[level - 1] == null) {
      level--;
    }
    elemSize--;
    return true;
  }

  public int size() {
    return elemSize;
  }

  public int[] toArray() {
    int[] array = new int[elemSize];
    SkipNode x = head.forward[0];
    for(int i = 0 ; i < elemSize; ++ i){
      array[i] = x.key;
      x = x.forward[0];
    }
    return array;
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("size=" + size() + "[");
    int[] array = this.toArray();
    for (int i = 0; i < size(); ++i) {
      if (i > 0) {
        builder.append(',');
      }
      builder.append(array[i]);
    }
    builder.append("]");
    return builder.toString();
  }
}
