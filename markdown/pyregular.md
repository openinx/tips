### what's this regular expression means ?
+  expression.1

```python

    _c2u = re.compile('(((?<=[a-z])[A-Z])|([A-Z](?![A-Z]|$)))')
    def camelcase_to_underscore(str):
        return _c2u.sub(r'_\1', str).lower().strip('_')

```
